### How to ripen the mangoes

##### Bring the store up
```bash
docker-compose up -d && docker-compose logs -f
```

##### Open the fridge to see the mangoes
```bash
docker run --rm -it --network mongo_baronet mongo:4.4.6 mongo "mongodb://mango:mango@mongo:27017/mango" --shell
docker run --rm -it --network mongo_baronet mongo:4.4.6 mongo "mongodb://root:volvo@mongo:27017/admin"
docker run --rm -it --network mongo_baronet mongo:4.4.6 mongo -u root -p volvo --host mongo --port 27017 admin
docker run --rm -it --network mongo_baronet mongo:4.4.6 mongo -u mango -p mango --host mongo --port 27017 mango --eval 'db.collection.find()'
```

##### Frequently used mongo shell commands
Sauce: https://docs.mongodb.com/manual/reference/mongo-shell/

Helpers:
```bash
help
help admin
db.help()
db.<collection>.help()
show dbs
use <db>
show collections
show users
show roles
db.dropDatabase()
```

Basic Shell JavaScript operations:
```bash
db.auth("root", "volvo")
alias = db.<collection>
alias.find()
db.collection.find()
db.collection.find({"key": "value"})
db.collection.insertOne({"key": "value"})
db.collection.updateOne({"key": "value"}, {$set: {"key": "another-value"}})
db.collection.updateOne({"key": "value"}, {$set: {"key2": "value2", "key3": "value3"}})
db.collection.deleteOne({"key": "value"})
db.collection.drop()
```
